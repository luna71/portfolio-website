// Dependencies
const express = require('express');
const path = require('path');
// Routes
const guestRouter = require('./routes/guest.js');

const app = express();
const port = process.env.PORT || 8080;


app.engine('jade', require('jade').__express);
app.set('view engine','pug');
app.set('views', path.join(__dirname, './../views'));


app.use('/', guestRouter);
app.use(express.static(__dirname + './../public'));
app.listen(port, ()=> console.log(`Server started on port ${port}`));